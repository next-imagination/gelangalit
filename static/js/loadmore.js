   $(function() {
      $('body').scrollTop(0);
   });
   $(document).ready(function() {
      $(".category").on('change', function() {
          const category = $(this).val();
          const query = $(".query").val();
          const url = "/search?query="+query+"&category="+category;
          window.location = url;
          
      });
      $("#historyBtn").on('click', function() {
          $('.loadmoreajaxloader').show();
          loadItems();
      });
   });
   
   function hideAllAttr() {
      $("#hsbutn").hide();
      $('.loadmoreajaxloader').hide();
      return true;
   }
   var pageNum = 1;
   var hasNextPage = true; 
   var loadItems = function() {
      if (hasNextPage === false) {
          return false;
      }    
      $("#hsbutn").hide();
      pageNum = pageNum + 1;
      $.ajax({
          url: $(location).attr('href'),
          data: {
              page_number: pageNum
          },
          success: function(data) {
              if ($(data).text() != "" ) {
                  $('.loadmoreajaxloader').hide();
                  $($(data).hide().fadeIn(2000)).appendTo($(".postwrapper")); 
                  hasNextPage = true;    
                  $("#hsbutn").show();     
              } else {
                  hasNextPage = false
                  hideAllAttr()
              }
          },error: function(data) {
              hasNextPage = false
          }
      });
   };
