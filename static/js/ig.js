/*!
 * jquery.instagramFeed
 *
 * @version 1.0
 *
 * @author Javier Sanahuja Liebana <bannss1@gmail.com>
 *
 * https://github.com/BanNsS1/jquery.instagramFeed
 *
 */
(function($) {
    var defaults = {
        'username': '',
        'container': '',
        'display_profile': true,
        'display_biography': true,
        'display_gallery': true,
        'get_raw_json': false,
        'callback': null,
        'styling': true,
        'items': 8,
        'items_per_row': 4,
        'margin': 0.5
    };
    $.instagramFeed = function(options) {
        options = $.fn.extend({}, defaults, options);
        var url = "/static/data/lokalexperience.json";
        $.get({url:url, cache: false
        }).then(function(data){
            var styles = {
                'profile_container': "",
                'profile_image': "",
                'profile_name': "",
                'profile_biography': "",
                'gallery_image': ""
            };
            if (options.styling) {
                var width = (100 - options.margin * 2 * options.items_per_row) / options.items_per_row;
                styles.gallery_image = " style='margin:" + options.margin + "% " + options.margin + "%;width:" + 30 + "%;float:left;'";

            }

            var html = "";
            if (options.display_gallery) {
                html += "<div class='instagram_gallery'>";
                var c = 1
                $.each(data["GraphImages"], function(i, item) {
                    var text = item.edge_media_to_caption.edges[0].node.text
                    if (text.indexOf(options.tag) != -1 && options.tag != "") {
                        if (c <= 12) {
                            var url = "https://www.instagram.com/p/" + item.shortcode;
                            html += "<a href='" + url + "' target='_blank'>";
                            html += "	<img src='" + item.thumbnail_src + "' alt='" + options.username + " instagram image " + i + "'" + styles.gallery_image + " />";
                            html += "</a>";
                        }
                        c += 1
                    }

                    html += "</div>";
                });
            }
            $(options.container).html(html);
        });
    };

})(jQuery);
