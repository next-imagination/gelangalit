from django.db import models
from django.contrib import admin
from django.template import defaultfilters


from behaviors.models import (
    human_format,
    ObjectManager,
    Timestampable,
    Authorable,
    Permalinkable,
    Nameable
)


class FaqPramuwisata(Timestampable, models.Model):
    title = models.CharField(blank=True, max_length=200)
    description = models.TextField(blank=True)
 

    def save(self, *args, **kwargs):
        super(FaqPramuwisata, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Faq Pramuwisata'

    def __str__(self):
        return self.title

admin.site.register(FaqPramuwisata)

class FaqWisatawan(Timestampable, models.Model):
    title = models.CharField(blank=True, max_length=200)
    description = models.TextField(blank=True)
 
    def save(self, *args, **kwargs):
        super(FaqWisatawan, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Faq Wisatawan'

    def __str__(self):
        return self.title

admin.site.register(FaqWisatawan)
