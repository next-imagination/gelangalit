from django.contrib import admin
from django.forms import ModelForm
from tour.models import TourPackage, Booking, Availability, Review
from django.utils.safestring import mark_safe
from django.template.loader import render_to_string, get_template


from django.core.urlresolvers import reverse

class ReviewAdmin(admin.ModelAdmin):
    fieldExclude = ["id", "created", "modified"]

    list_display = [
        field.name for field in Review._meta.fields if field.name not in fieldExclude]
    list_filter = ("tour", "blocked")
    search_fields = ('tour__title',)
    ordering = ('-id',)
    #readonly_fields = ["price"]

    def changelist_view(self, request, extra_context=None):
        extra_context = {
            'title': 'List Data Review Experience. Cari, Ubah dan Delete.'}
        return super(ReviewAdmin, self).changelist_view(request, extra_context=extra_context)


admin.site.register(Review, ReviewAdmin)

class AvailabilityAdmin(admin.ModelAdmin):
    fieldExclude = ["id", "created", "modified"]

    list_display = [
        field.name for field in Availability._meta.fields if field.name not in fieldExclude]
    list_filter = ("start_date", "blocked", "tour")
    search_fields = ('tour__title',)
    ordering = ('-id',)
    #readonly_fields = ["price"]

    def changelist_view(self, request, extra_context=None):
        extra_context = {
            'title': 'List Data Available Date. Cari, Ubah dan Delete.'}
        return super(AvailabilityAdmin, self).changelist_view(request, extra_context=extra_context)


admin.site.register(Availability, AvailabilityAdmin)

class TourAdmin(admin.ModelAdmin):
    fieldExclude = ["id", "host", "secondary_category", "gallery1", "gallery2", "gallery3", "gallery4", 
                    "gallery4", "gallery5", "gallery6", "gallery7", "gallery8", "gallery9", "address",
                    "maxgroup", "age", "igtag", "type_expt", "about", "activity", "access", "guestbring", "note", "add_requirements",
                    "mp_description", "lat", "lang", "bkeyword", "start_time", "lang_level", "end_time",
                    "slug", "created", "modified", "languages", "photo", "provide_food", "pfood_description",
                    "provide_drinks", "drinks_description", "provide_tickets", "tickets_description", "provide_transport",
                    "transport_description", "provide_equip", "equip_description"]

    list_display = [
        field.name for field in TourPackage._meta.fields if field.name not in fieldExclude]
    #list_display += ["lphoto"]
    list_filter = ("category", "city", "active")
    search_fields = ('title',)
    ordering = ('-id',)

    def changelist_view(self, request, extra_context=None):
        extra_context = {
            'title': 'List Data Experience. Cari, Ubah dan Delete.'}
        return super(TourAdmin, self).changelist_view(request, extra_context=extra_context)

    #def lphoto(self, obj):
    #    return render_to_string('thumb.html', {
    #        'image': obj.photo
    #    })

    #lphoto.allow_tags = True

    #def photo_image(self, obj):
    #    return mark_safe('<img src="{url}" width="800"/>'.format(
    #        url=obj.photo.url,
    #    )
    #    )

admin.site.register(TourPackage, TourAdmin)

'''
class BookingAdmin(admin.ModelAdmin):
    list_display = ["created", "id_order", "order_id", "tour_title", "date_booking", "name", "contact", "email", "guest"]  
    list_filter = ("dates",)
    search_fields = ('name','order_id', 'email')
    ordering = ('-id',)
    fieldNotProtect = ["status"]
    readonly_fields = [field.name for field in Booking._meta.fields if field.name not in fieldNotProtect]

    def changelist_view(self, request, extra_context=None):
        extra_context = {
            'title': 'List Data Booking. Cari, Ubah dan Delete.'}
        return super(BookingAdmin, self).changelist_view(request, extra_context=extra_context)

    def date_booking(self, obj):
        return "%s" % obj.dates.strftime('%A %d-%m-%y')

    def id_order(self, obj):
        return "%s" % obj.id

    def tour_title(self, obj):
        #from django.shortcuts import resolve_url
        #from django.contrib.admin.templatetags.admin_urls import admin_urlname
        #url = resolve_url(admin_urlname(TourPackage._meta, 'change'), obj.tour.id)
        return mark_safe('<a href="/experience/{slug}">{title}</a>'.format(slug=obj.tour.slug, title=str(obj.tour.title)))


    def get_actions(self, request):
        #Disable delete
        actions = super(BookingAdmin, self).get_actions(request)
        del actions['delete_selected']
        return actions

    def has_delete_permission(self, request, obj=None):
        #Disable delete
        return False
'''
admin.site.register(Booking)
