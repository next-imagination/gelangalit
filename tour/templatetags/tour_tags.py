from django import template
import urlparse
from behaviors.models import human_format
register = template.Library()

def HumanFormat(price):
    return human_format(price)

register.filter('HumanFormat', HumanFormat)


