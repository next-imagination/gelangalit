from behaviors.models import (
    human_format,
    ObjectManager,
    Timestampable,
    Authorable,
    Permalinkable,
    Nameable
)
from django.db import models
from django.contrib import admin
from decimal import Decimal
import datetime
import os
import ast
from django.template import defaultfilters
from datetime import datetime as dt
from django.utils import timezone
from tinymce.models import HTMLField
from babel.numbers import format_number, format_decimal, format_percent
import requests
from django.db.models.signals import pre_delete, pre_save
from django.dispatch.dispatcher import receiver
from django.utils.functional import cached_property
from multiselectfield import MultiSelectField
from django.core.files.storage import default_storage

TELEGRAM_TOKEN = '743973008:AAEYaIq59sRLQIZ6Z1OaFYqCkr5c6iYZgaY'
TELEGRAM_CHAT_ID = '-311049375'


class State(Nameable, Timestampable, Permalinkable, models.Model):
    url_name = "state"

    @property
    def slug_source(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = defaultfilters.slugify(self.name)
        super(State, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Wilayah'

    def __str__(self):
        return self.name


admin.site.register(State)


def get_city_upload(instance, filename):
    return os.path.join(
        'city/picture/',
        dt.now().date().strftime("%Y/%m/%d"),
        filename)


class City(Nameable, Timestampable, Permalinkable, models.Model):
    state = models.ForeignKey(State, blank=True)
    description = models.TextField(blank=True, max_length=300)
    photo = models.ImageField('Photo', upload_to=get_city_upload, blank=True)


    url_name = "city"

    @property
    def slug_source(self):
        return self.name

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = defaultfilters.slugify(self.name)
        super(City, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Kota'


admin.site.register(City)


def get_category_upload(instance, filename):
    return os.path.join(
        'category/picture/',
        dt.now().date().strftime("%Y/%m/%d"),
        filename)


class Category(Nameable, Timestampable, Permalinkable, models.Model):
    url_name = "category"
    description = models.TextField(blank=True, max_length=300)
    photo = models.ImageField(
        'Photo',
        upload_to=get_category_upload,
        blank=True)

    parent = models.ForeignKey(
        'self',
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name='children')
    
    @property
    def slug_source(self):
        return self.name

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = defaultfilters.slugify(self.name)
        super(Category, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Kategori Experience'


admin.site.register(Category)


def get_gallery_upload(instance, filename):
    return os.path.join(
        'tourpackage/gallery/',
        dt.now().date().strftime("%Y/%m/%d"),
        filename)


def get_tpict_upload(instance, filename):
    return os.path.join(
        'tourpackage/picture/',
        dt.now().date().strftime("%Y/%m/%d"),
        filename)


def get_tcover_upload(instance, filename):
    return os.path.join(
        'tourpackage/cover/',
        dt.now().date().strftime("%Y/%m/%d"),
        filename)


def get_host_upload(instance, filename):
    return os.path.join(
        'tourpackage/hosts/',
        dt.now().date().strftime("%Y/%m/%d"),
        filename)


class TourPackage(Timestampable, Authorable, Permalinkable, models.Model):
    FOOD = (('1', 'Camilan'),
            ('2', 'Hidangan pembuka'),
            ('3', 'Sarapan'),
            ('4', 'Makan siang'),
            ('5', 'Makan malam'),
            ('6', 'Pencuci mulut'),
            ('7', 'Menu uji rasa'),
            )

    DRINKS = (('1', 'Minuman beralkohol'),
              ('2', 'Bir'),
              ('3', 'Koktail'),
              ('4', 'Kopi'),
              ('5', 'Jus'),
              ('6', 'Minuman ringan'),
              ('6', 'Minuman keras'),
              ('7', 'Teh'),
              ('8', 'Air'),
              ('9', 'Anggur'),
              )

    TICKETS = (('1', 'Tiket acara'),
               ('2', 'Tiket pertunjukan'),
               ('3', 'Biaya masuk')
               )

    TRANSPORTATION = (('1', 'Bus'),
                      ('2', 'Kereta api'),
                      ('3', 'Mobil'),
                      ('4', 'Sepeda'),
                      ('5', 'Perahu'),
                      ('6', 'Kapal pesiar'),
                      ('6', 'Feri'),
                      ('7', 'Helikopter'),
                      ('8', 'Balon udara'),
                      ('9', 'Pesawat terbang'),
                      ('10', 'Sepeda motor'),
                      ('11', 'Off road'),
                      ('12', 'Skuter')
                      )

    EQUIPMENT = (('1', 'Peralatan olahraga'),
                 ('2', 'Perlengkapan luar ruangan'),
                 ('3', 'Perlengkapan keselamatan'),
                 ('4', 'Perlengkapan kreatif'),
                 ('5', 'Kamera'),
                 ('6', 'Fotografi')
                 )

    TYPEEXP = (('1', 'Tidak, pengalaman ini untuk semua orang'),
               ('2', 'Ya, pengalaman ini untuk jenis wisatawan tertentu')
               )

    ACTIVITY_LEVEL = (('1', 'Ringan'),
                      ('2', 'Sedang'),
                      ('3', 'Berat'),
                      ('4', 'Ekstrem')
                      )
    GUEST_LEVEL = (('1', 'Pemula'),
                   ('2', 'Menengah')
                   )
    title = models.CharField(blank=True, max_length=52)
    category = models.ForeignKey(
        Category,
        blank=True,
        null=True,
        help_text="Pikirkan apa yang ingin orang-orang dapatkan dari pengalaman Anda. Aktivitas Anda akan dipusatkan di sekitar tema ini.")
    secondary_category = models.ForeignKey(
        Category,
        blank=True,
        null=True,
        related_name="secondary_category",
        help_text="Pengalaman paling unik memiliki lebih dari satu tema. Tangkap berbagai minat dengan menyertakan lebih banyak tema.")

    city = models.ForeignKey(
        City,
        blank=True,
        help_text="Di kota mana Anda akan menyelenggarakan pengalaman?")
    type_expt = models.CharField(
        "Tipe experience",
        choices=TYPEEXP,
        max_length=1,
        default=1,
        help_text="Ini akan membantu kami menampilkan pengalaman Anda kepada jenis wisatawan yang tepat.")

    # Specific about guest type
    foreign = models.BooleanField(default=False)
    local = models.BooleanField(default=False)
    about = models.TextField(
        blank=True, verbose_name="Tentang Host", help_text="\
    Ceritakan lebih banyak mengenai diri Anda<br>\
    Kualifikasi Anda akan menunjukkan mengapa Anda orang yang paling tepat untuk menyelenggarakan pengalaman ini.<br>\
    Luangkan waktu untuk memberi tahu tamu mengapa Anda sangat antusias dan tahu banyak mengenai topik tersebut.<br>\
    <br>Tips:<br>\
    + Menonjolkan kredensial unik yang menjadikan Anda cocok untuk mengadakan aktivitas ini<br>\
    + Sebutkan berapa tahun Anda telah melakukan aktivitas tersebut<br>\
    + Beri tahu semua orang mengapa Anda sangat antusias mengadakan aktivitas ini")
    host = models.ImageField('Host', upload_to=get_host_upload, blank=True)
    host_name = models.CharField(max_length=100, blank=True)
    activity = models.TextField(
        blank=True, verbose_name="Yang kita lakukan", help_text="\
    Sebutkan kegiatan yang akan dilakukan<br>\
    Deskripsi aktivitas Anda adalah kesempatan untuk menarik tamu lain agar mencoba pengalaman Anda. <br>\
    Ceritakan detail jadwal perjalanan yang telah Anda siapkan untuk mereka.<br><br>\
    + Memberikan penjelasan spesifik mengenai apa yang akan dilakukan para tamu dalam aktivitas Anda<br>\
    + Merencanakan jadwal perjalanan yang terperinci agar tamu tahu apa yang akan mereka dapatkan<br>\
    + Memberikan akses khusus kepada tamu ke sesuatu yang tak dapat mereka temukan sendiri<br>")

    access = models.TextField(
        blank=True, verbose_name="Dimana kita akan berada", help_text="\
    Jelaskan setiap tempat yang akan dikunjungi. \
    <br>Jika pengalaman Anda mencakup beberapa tempat, jelaskan setiap tempat yang akan Anda kunjungi.  \
    <br>Anda tidak perlu menyertakan alamat di sini. Tamu akan menerima email berisi informasi lengkap nantinya. \
    <br>Tips:<br>\
    + Sebutkan tempat-tempat spesial yang akan Anda kunjungi<br>\
    + Memberi tamu akses ke tempat-tempat istimewa yang hanya diketahui oleh warga lokal")

    provide_food = MultiSelectField(choices=FOOD, null=True, blank=True)
    pfood_description = models.TextField(
        blank=True,
        max_length=300,
        verbose_name="Makanan/Snacks jika ada")

    provide_drinks = MultiSelectField(choices=DRINKS, null=True, blank=True)
    drinks_description = models.TextField(
        blank=True, max_length=300, verbose_name="Minuman jika ada")

    provide_tickets = MultiSelectField(
        choices=TICKETS,
        verbose_name="Tiket jika ada",
        null=True,
        blank=True)
    tickets_description = models.TextField(
        blank=True, max_length=300, verbose_name="Tiket jika ada")

    provide_transport = MultiSelectField(
        choices=TRANSPORTATION,
        verbose_name="Transportasi jika ada",
        null=True,
        blank=True)
    transport_description = models.TextField(
        blank=True, max_length=300, verbose_name="Transportasi jika ada")

    provide_equip = MultiSelectField(
        choices=EQUIPMENT,
        verbose_name="Perlengkapan jika ada",
        null=True,
        blank=True)
    equip_description = models.TextField(
        blank=True,
        max_length=300,
        verbose_name="Deskripsi Perlengkapan")

    guestbring = models.TextField(
        blank=True, verbose_name="Apa saja yang harus dibawa tamu?", help_text="\
    Pikirkan apa saja yang tidak Anda sediakan dalam pengalaman ini dan harus dibawa sendiri oleh tamu. <br>\
    Informasi ini akan diberitahukan kepada tamu melalui email begitu mereka memesan.")

    note = models.TextField(
        blank=True,
        verbose_name="Apa lagi yang perlu diketahui tamu sebelum mereka memesan?",
        help_text="\
    Tempatkan diri Anda dalam posisi tamu. Beberapa informasi mungkin sudah jelas,<br>\
    tetapi berikanlah informasi yang terperinci agar tamu memiliki persiapan yang matang.<br>")

    photo = models.ImageField(
        'Photo depan',
        upload_to=get_tpict_upload,
        null=True, blank=True)

    gallery1 = models.ImageField(
        'Tuan Rumah',
        upload_to=get_gallery_upload,
        blank=True,
        help_text="Perlihatkan diri Anda saat sedang memandu aktivitas")
    gallery2 = models.ImageField(
        'Kegiatan',
        upload_to=get_gallery_upload,
        blank=True,
        help_text="Perlihatkan tamu yang sedang mengikuti pengalaman Anda")
    gallery3 = models.ImageField(
        'Detail',
        upload_to=get_gallery_upload,
        blank=True,
        help_text="Ambil foto close-up dari berbagai tekstur atau detail menarik")
    gallery4 = models.ImageField(
        'Lokasi',
        upload_to=get_gallery_upload,
        blank=True,
        help_text="Tunjukkan pemandangan lengkap dan coba sertakan orang-orang")
    gallery5 = models.ImageField(
        'Lain-lain',
        upload_to=get_gallery_upload,
        blank=True,
        help_text="lainnya jika ada")
    gallery6 = models.ImageField(
        'Lain-lain',
        upload_to=get_gallery_upload,
        blank=True,
        help_text="lainnya jika ada")
    gallery7 = models.ImageField(
        'Lain-lain',
        upload_to=get_gallery_upload,
        blank=True,
        help_text="lainnya jika ada")
    gallery8 = models.ImageField(
        'Lain-lain',
        upload_to=get_gallery_upload,
        blank=True,
        help_text="lainnya jika ada")
    gallery9 = models.ImageField(
        'Lain-lain',
        upload_to=get_gallery_upload,
        blank=True,
        help_text="lainnya jika ada")

    address = models.TextField(
        verbose_name="Lokasi pertemuan",
        blank=True,
        max_length=500,
        help_text="Di mana sebaiknya tamu bertemu dengan Anda?")
    lat = models.CharField(max_length=100, blank=True)
    lang = models.CharField("long", max_length=100, blank=True)

    age = models.PositiveIntegerField(
        verbose_name="Usia minimum",
        help_text="Tetapkan batasan usia untuk tamu. Anak di bawah umur hanya bisa ikut jika ditemani oleh wali mereka yang sah.",
        default=0)
    maxgroup = models.PositiveIntegerField(
        verbose_name="Jumlah peserta", default=0)
    activity_level = models.CharField(
        "Bagaimana tingkat aktivitas yang perlu diantisipasi peserta?",
        choices=ACTIVITY_LEVEL,
        max_length=1,
        default=1,
        help_text="Pikirkan seberapa aktif peserta akan bergerak sepanjang pengalaman Anda.")
    guest_level = models.CharField(
        "Bagaimana tingkat kemahiran yang dibutuhkan?",
        choices=GUEST_LEVEL,
        max_length=1,
        default=1,
        help_text="\
    Pikirkan tingkat pengalaman yang perlu dimiliki tamu untuk mengikuti aktivitas Anda.<br>")
    additional_req = models.TextField(
        blank=True, verbose_name="Persyaratan tambahan (opsional)", help_text="\
    Contohnya, tamu harus memiliki pengalaman berselancar sebelumnya, tamu harus memegang lisensi selam skuba, tamu harus nyaman berada di dekat anjing, dll.<br>")

    duration = models.CharField(
        max_length=5,
        blank=True,
        verbose_name="Durasi")
    start_time = models.TimeField(
        blank=True, null=True, help_text="Masukan waktu trip berakhir")
    end_time = models.TimeField(
        blank=True, null=True, help_text="Masukan waktu trip berakhir")

    igtag = models.CharField("TagInstagram", max_length=100, blank=True)

    price = models.DecimalField(
        verbose_name="Tetapkan harga per tamu",
        null=True,
        blank=True,
        max_digits=15,
        decimal_places=2)
    active = models.BooleanField(default=True)
    url_name = "experience"
    objects = ObjectManager()

    @property
    def slug_source(self):
        return self.title

    def get_availability(self):
        import ast
        data = self.availability_set.all()
        listd = []
        list_av = []
        days = []
        other_guest = []
        js = ""
        for i in data:
            if i.days:
                days = ast.literal_eval(i.days)
            if i.blocked:
                getlist = [(i.start_date + datetime.timedelta(n)).strftime("%d/%m/%Y")
                           for n in range(int((i.end_date - i.start_date).days) + 1)]
                listd = list(set(listd + getlist))
            else:
                getlist = [(i.start_date + datetime.timedelta(n)).strftime("%d/%m/%Y")
                           for n in range(int((i.end_date - i.start_date).days) + 1)]
                list_av = list(set(list_av + getlist))

        for i in self.booking_set.all():
            if self.booking_set.filter(dates=i.dates).count() >= self.maxgroup:
               other_guest.append(i.dates.strftime("%d/%m/%Y"))

        if days:
            js += "daysOfWeekDisabled: %s," % days
        if listd:
            listd = list(set(listd) - set(list_av))
            listd = list(set(listd) - set(other_guest))
            js += "datesDisabled: %s," % listd
        if other_guest and not listd:
            js += "datesDisabled: %s," % other_guest

        return js

    def get_category(self):
        return self.category.name

    def get_photo(self):
        return self.photo.url

    def get_host_picture(self):
        return self.host.url

    def get_map_url(self):
        mapurl = "https://www.google.com/maps?saddr=My+Location&daddr=%s,%s" % (
            self.lat, self.lang)
        return mapurl

    def get_price(self):
        return format_decimal(self.price, locale='ID')

       
    def todatetime(self, time):
        return datetime.datetime.today().replace(hour=time.hour, minute=time.minute, second=time.second, 
                                             microsecond=time.microsecond, tzinfo=time.tzinfo)

    def second_to_hours(self, seconds):
        hour =str(seconds//3600)
        sec =str((seconds%3600)//60)
        result = "%s jam" % hour
        if sec != "0":
           result = "%s jam %s menit" % (hour, sec)
        return result

    def get_duration(self):
        compare_time = (self.todatetime(self.end_time) - self.todatetime(self.start_time)).seconds
        return self.second_to_hours(compare_time)

    def __str__(self):
        if self.active:
            return "[ACTIVE] %s" % (self.title)
        return "[DISABLE] %s" % (self.title)

    
    def save(self, *args, **kwargs):
        if self.title:
            self.slug = defaultfilters.slugify(self.title)

        super(TourPackage, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Experience'


class Availability(Timestampable, models.Model):
    tour = models.ForeignKey(TourPackage, blank=True, null=True)
    start_date = models.DateField(
        blank=True, null=True, help_text="Masukan waktu trip dimulai")
    end_date = models.DateField(
        blank=True, null=True, help_text="Masukan waktu trip berakhir")
    start_time = models.TimeField(
        blank=True, null=True, help_text="Masukan waktu trip berakhir")
    end_time = models.TimeField(
        blank=True, null=True, help_text="Masukan waktu trip berakhir")
    #cashback = models.BooleanField(default=False)
    blocked = models.BooleanField(default=True)
    days = models.CharField(max_length=100, blank=True)
    price = models.DecimalField(
        null=True,
        blank=True,
        max_digits=15,
        decimal_places=2)
        
    objects = ObjectManager()

    def __str__(self):
        return "%s" % (self.id)

    def get_price(self):
        return human_format(self.price)

    class Meta:
        verbose_name_plural = 'Tersedianya Tanggal'


class Review(Timestampable, models.Model):
    STAR = (
        ('1', 1),
        ('2', 2),
        ('3', 3),
        ('4', 4),
        ('5', 5),
    )
    tour = models.ForeignKey(TourPackage, blank=True, null=True)
    message = models.TextField(blank=True, null=True)
    name = models.CharField(max_length=100, blank=True)
    star = models.CharField(choices=STAR, max_length=2)
    blocked = models.BooleanField(default=False)
    objects = ObjectManager()

    def __str__(self):
        return self.tour.title

    def get_star(self):
        star_tag = '<i class="icon_star voted"></i>'
        list_star = ''
        for i in range(int(self.star)):
            list_star += star_tag
        return list_star

    class Meta:
        verbose_name_plural = 'Review Experience'


class Booking(Timestampable, models.Model):
    STATUS = (
        ('1', "UNPAID"),
        ('2', "PAID"),
        ('3', "EXPIRED")
    )
    tour = models.ForeignKey(TourPackage, blank=True, null=True)
    available = models.ForeignKey(
        Availability,
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
    )

    order_id = models.CharField(
        max_length=20, blank=True, null=True, unique=True)
    name = models.CharField(blank=True, null=True, max_length=150)
    contact = models.TextField(blank=True, null=True)
    dates = models.DateField(blank=True, null=True,
                             help_text="Masukan waktu trip dimulai")

    guest = models.PositiveIntegerField(help_text="guest", default=0)
    email = models.CharField(blank=True, null=True, max_length=100)
    price = models.DecimalField(
        null=True,
        blank=True,
        max_digits=15,
        decimal_places=2)

    status = models.CharField(choices=STATUS, default="1", max_length=2)
    payment_id = models.CharField(blank=True, null=True, max_length=30)
    payment_method = models.CharField(blank=True, null=True, max_length=50)
    paid_amount = models.DecimalField(
        null=True,
        blank=True,
        max_digits=15,
        decimal_places=2)

    adjusted_received_amount =  models.DecimalField(
        null=True,
        blank=True,
        max_digits=15,
        decimal_places=2)

    paid_date = models.DateTimeField(blank=True, null=True)
    json_objects = models.TextField(blank=True, null=True)
    objects = ObjectManager()

    @property
    def slug(self):
        return defaultfilters.slugify(self.name)

    def get_link(self):
        url = "https://lokalexperience.com/booking?id=" + self.order_id
        return url

    @property
    def get_price_list(self):
        price_per = self.available.price * Decimal(self.guest)
        percentage = 10000
        price = price_per + percentage
        return format_decimal(price, locale='ID')

    @property
    def get_price_raw(self):
        price_per = self.available.price * Decimal(self.guest)
        percentage = 10000
        price = price_per + percentage
        return price

    def __str__(self):
        return "%s" % (self.name)

    class Meta:
        verbose_name_plural = 'Data Booking Wisatawan'


@receiver(pre_save, sender=Availability)
def checking_field_av(sender, instance, *args, **kwargs):
    if not instance.price:
        instance.price = instance.tour.price

    if not instance.start_time:
        instance.start_time = instance.tour.start_time

    if not instance.end_time:
        instance.end_time = instance.tour.end_time


@receiver(pre_save, sender=Booking)
def SendToTelegram(sender, instance, *args, **kwargs):
    if not instance.pk:
        return
    try:
        text = "[Booking 00%s:%s] YAAY ada yang booking. cek detail booking di url %s" % (
            instance.id, instance.name, instance.get_link())
        payload = {
            'chat_id': TELEGRAM_CHAT_ID,
            'text': text,
            'parse_mode': 'HTML'
        }
        requests.post(
            "https://api.telegram.org/bot{token}/sendMessage".format(
                token=TELEGRAM_TOKEN),
            data=payload).content
    except Exception:
        return False
    return True


@receiver(pre_delete, sender=TourPackage)
def TourMainDel(sender, instance, **kwargs):
    try:
        default_storage.delete(instance.host.name)
        default_storage.delete(instance.photo.name)
        default_storage.delete(instance.gallery1.name)
        default_storage.delete(instance.gallery2.name)
        default_storage.delete(instance.gallery3.name)
        default_storage.delete(instance.gallery4.name)
        default_storage.delete(instance.gallery5.name)
        default_storage.delete(instance.gallery6.name)
        default_storage.delete(instance.gallery7.name)
        default_storage.delete(instance.gallery8.name)
        default_storage.delete(instance.gallery9.name)
        instance.clean()
    except Exception:
        return False
    return True


@receiver(pre_save, sender=TourPackage)
def TourPicChange(sender, instance, *args, **kwargs):
    if not instance.pk:
        return

    try:
        obj = sender.objects.get(id=instance.id)
    except sender.DoesNotExist:
        # object is not in db, nothing to worry about
        return
        # is the save due to an update of the actual image file?

    if obj.host and instance.host and obj.host != instance.host:
        default_storage.delete(obj.host.name)
    if obj.photo and instance.photo and obj.photo != instance.photo:
        default_storage.delete(obj.photo.name)
    if obj.gallery1 and instance.gallery1 and obj.gallery1 != instance.gallery1:
        default_storage.delete(obj.gallery1.name)

    if obj.gallery2 and instance.gallery2 and obj.gallery2 != instance.gallery2:
        default_storage.delete(obj.gallery2.name)

    if obj.gallery3 and instance.gallery3 and obj.gallery3 != instance.gallery3:
        default_storage.delete(obj.gallery3.name)

    if obj.gallery4 and instance.gallery4 and obj.gallery4 != instance.gallery4:
        default_storage.delete(obj.gallery4.name)

    if obj.gallery5 and instance.gallery5 and obj.gallery5 != instance.gallery5:
        default_storage.delete(obj.gallery5.name)

    if obj.gallery6 and instance.gallery6 and obj.gallery6 != instance.gallery6:
        default_storage.delete(obj.gallery6.name)

    if obj.gallery7 and instance.gallery7 and obj.gallery7 != instance.gallery7:
        default_storage.delete(obj.gallery7.name)

    if obj.gallery8 and instance.gallery8 and obj.gallery8 != instance.gallery8:
        default_storage.delete(obj.gallery8.name)

    if obj.gallery9 and instance.gallery9 and obj.gallery9 != instance.gallery9:
        default_storage.delete(obj.gallery9.name)
    return
