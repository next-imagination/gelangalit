from django.conf.urls import include, url
from django.conf.urls.static import static
from django.conf import settings
from django.http import HttpResponse
from django.views.generic import TemplateView
from django.contrib import admin
from django.views.generic.base import RedirectView
from django.views.decorators.csrf import csrf_exempt
from django.views.defaults import page_not_found

from main.views import (
    HomePageView, 
    TourDetail,
    BookingView,
    ArticleView,
    ArticleDetail,
    AboutView,
    ContactView,
    SearchView,
    JoinLokalHost,
    Policy,
    Faqs,
    BlogCategory,
    Invoice,
    Cancellation,
    SuccessPayment
)

urlpatterns = [
    # Examples:
    url(r'^$', HomePageView.as_view(), name='homepageview'),
    url(r'^bergabung-lokal$', JoinLokalHost.as_view(), name='joinlokalhost'),
    url(r'^kebijakan-privasi$', Policy.as_view(), name='privacypolicy'),
    url(r'^kebijakan-pembatalan$', Cancellation.as_view(), name='cancellation'),
    
    url(r'^bantuan$', Faqs.as_view(), name='faq'),
    url(r'^booking/$', Invoice.as_view(), name='invoice'),
 
    url(r'^blog$', ArticleView.as_view(), name='articles'),
    url(r'^blog/category/(?P<slug>[\w\d\-\.]+)$', BlogCategory.as_view(), name='category'),
    url(r'^tentang$', AboutView.as_view(), name='about'),
    url(r'^search$', SearchView.as_view(), name='search'),
    
    url(r'^kontak$', ContactView.as_view(), name='contact'),
    url(r'^blog/(?P<slug>[\w\d\-\.]+)$', ArticleDetail.as_view(), name='article_detail'),

    url(r'^experience/(?P<slug>[\w\d\-\.]+)$', TourDetail.as_view(), name='experience'),
    url(r'^experience/(?P<slug>[\w\d\-\.]+)/post$', BookingView.as_view(), name='bookingexp'),

    url(r'^success-payment$', csrf_exempt(SuccessPayment.as_view()), name='successpayment'),
    url(r'^404/$', page_not_found, {'exception': Exception()}),
    #url(r'^favicon\.ico$', RedirectView.as_view(url='/static/icons/favicon.ico', permanent=True)),
    url(r'^tinymce/', include('tinymce.urls'))
] 



admin.autodiscover()
urlpatterns += [
    url(r'^admin/', include(admin.site.urls)),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),

    ] + urlpatterns


    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    
admin.site.site_header = "Lokalx.id Admin"
admin.site.site_title = "Lokalx.id Admin Portal"
admin.site.index_title = "Welcome to Lokalx.id Admin Portal"
