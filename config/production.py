from config.base import *

DEBUG = False
#MEDIA_URL = '/media/'

import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

sentry_sdk.init(
    dsn="https://5966b8f90aa04e7d903723e187fffe73@sentry.io/1858462",
    integrations=[DjangoIntegration()]
)

ALLOWED_HOSTS = ["lokalx.id", "https://lokalx.id"]
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'lokalx',
        'USER': 'jimmy',
        'PASSWORD': 'Temporary123!',
        'HOST': '192.168.88.83',
        'PORT': '5432',
    }
}
