import os, getpass
from django.core.exceptions import ImproperlyConfigured
from django.utils.translation import ugettext_lazy as _
from datetime import datetime, timedelta

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PROJECT_PATH = os.path.abspath(os.path.dirname(__file__))
SITE_ROOT = os.path.realpath(os.path.dirname(__file__))

def get_env_variable(var_name):
    try:
       return os.environ[var_name]
    except KeyError:
       error_msg = "Set the {} environment variable".format(var_name)
    raise ImproperlyConfigured(erro.r_msg)

SECRET_KEY = '824s8%^pp3q7_a1giq66wpu65tz(f4y1$via__k^3p6j*ju4f7'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        'ATOMIC_REQUESTS': False,
    }
}

INSTALLED_APPS = (
    'flat_responsive',
    'flat',        
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'translation_manager',
    'multiselectfield',
    'sorl.thumbnail',
    'tour',
    'behaviors',
    'main',
    'tinymce',
    'blog',
    'faq',
    'telegram_handler',
    'analytical',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

LANGUAGES = (
    ('en', _('English')),
    ('id-ID', _('Indonesia'))
)

USE_I18N = True
USE_L10N = True
LANGUAGE_CODE = 'id-ID'

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

ROOT_URLCONF = 'config.urls'
WSGI_APPLICATION = 'config.wsgi.application'


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(PROJECT_PATH, '../templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.template.context_processors.media',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages'
           ],
        },
    },
]

# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'Asia/Jakarta'
USE_I18N = True
USE_L10N = True
USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

#MEDIA_ROOT = os.path.join(SITE_ROOT, 'media')
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
#MEDIA_ROOT = "/home/jimmy/myproject/project/gelangalit"
MEDIA_URL = "/media/"

STATIC_ROOT = os.path.join(SITE_ROOT, 'static')
STATIC_URL = '/static/'
STATICFILES_DIRS = (
    'static',
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

TELEGRAM_TOKEN = '743973008:AAEYaIq59sRLQIZ6Z1OaFYqCkr5c6iYZgaY'
TELEGRAM_CHAT_ID = '-311049375'
GOOGLE_ANALYTICS_PROPERTY_ID = 'UA-129671097-1'


LOGGING = {
    'version': 1,
    'handlers': {
        'telegram': {
            'class': 'telegram_handler.TelegramHandler',
            'token': TELEGRAM_TOKEN,
            'chat_id': TELEGRAM_CHAT_ID
        }
    },
    'loggers': {
        'my_logger': {
            'handlers': ['telegram'],
            'level': 'DEBUG'
        }
    }
}
