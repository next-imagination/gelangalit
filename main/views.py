# -*- coding: utf-8 -*-
from django.core.paginator import EmptyPage, Paginator, InvalidPage
from django.utils.dateparse import parse_date
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from django.views.generic.base import TemplateView
from django.shortcuts import render, get_object_or_404, redirect
import re
from django.db.models import Count
from urllib import quote
import unicodedata
from django.db.models import Q
import time
from django.views.decorators.cache import never_cache
from django.template.loader import get_template
from behaviors.models import human_format
from tour.models import TourPackage, Booking, Category, Availability, City
from blog.models import Article, CategoryArticle
import uuid
import simplejson
import random
import string
import unicodedata
import datetime
import dateutil.parser
from datetime import datetime as dt
from babel.numbers import format_number, format_decimal, format_percent
from decimal import Decimal
import requests
import ast
from faq.models import FaqPramuwisata, FaqWisatawan
from django.utils import translation


def key_generator(limit=10):
    uuid_set = str(uuid.uuid4().fields[-1])[:5]
    d = [random.choice(string.digits + uuid_set)
         for x in xrange(limit)]
    key = "".join(d)
    return key


class HomePageView(View):
    template_name = 'main/index.html'

    def get(self, request, *args, **kwargs):
        qs = TourPackage.objects.select_related("city", "city__state", "category").filter(
            active=True).order_by('-id')

        objects = qs[:8]
        user_language = 'id'  # < this
        translation.activate(user_language)  # < this
        # < this
        request.session[translation.LANGUAGE_SESSION_KEY] = user_language
        city = City.objects.get(name="Banyuwangi")
        context = dict(
            objects=objects,
            terbaru=True,
            city=city,
            cat = Category.objects.all()
        )

        return render(request, self.template_name, context)


class SearchView(View):

    def normalize_query(self, query_string,
                        findterms=re.compile(r'"([^"]+)"|(\S+)').findall,
                        normspace=re.compile(r'\s{2,}').sub):
        return [normspace(' ', (t[0] or t[1]).strip())
                for t in findterms(query_string)]

    def get_query(self, query_string, search_fields):
        query = None
        terms = self.normalize_query(query_string)
        for term in terms:
            or_query = None
            for field_name in search_fields:
                q = Q(**{"%s__icontains" % field_name: term})
                if or_query is None:
                    or_query = q
                else:
                    or_query = or_query | q
            if query is None:
                query = or_query
            else:
                query = query & or_query
        return query

    def get(self, request, *args, **kwargs):
        user_language = 'id'  # < this
        translation.activate(user_language)  # < this
        # < this
        request.session[translation.LANGUAGE_SESSION_KEY] = user_language

        data = request.GET
        keyword = data.get('query', None)
        category = data.get('category', None)
        city = data.get('city', None)
        objects = []
        if keyword:
            keyword = unicodedata.normalize('NFKD', unicode(
                keyword)).encode('ascii', 'ignore').lower()
            entry_query = self.get_query(keyword, ['title'])
            entry_query2 = self.get_query(keyword, ['activity'])
            entry_query3 = self.get_query(keyword, ['city__name'])
            entry_query4 = self.get_query(keyword, ['city__state__name'])
            objects = TourPackage.objects.only('id', 'slug', 'title', 'photo', "city", "city__state", "price", "category").select_related("city").select_related(
                "city__state").select_related("category").order_by('-id').filter(entry_query | entry_query2 | entry_query3 | entry_query4, active=True)

        if city:
            if objects:
                objects = objects.filter(city__slug=city, active=True)
            else:
                objects = TourPackage.objects.filter(
                    city__slug=city, active=True)

        if category and category != "all":
            if objects:
                objects = objects.filter(category__slug=category, active=True)
            else:
                if not keyword:
                    objects = TourPackage.objects.filter(
                        category__slug=category, active=True)

        if not keyword and not category:
            objects = TourPackage.objects.only(
                'id', 'slug', 'title', 'photo').filter(
                active=True).order_by('-id')

        objects = objects.distinct()
        counting = objects.count()
        paginator = Paginator(objects, 24)
        if request.GET.get('page_number'):
            page_number = request.GET.get('page_number')
            try:
                page_objects = paginator.page(page_number).object_list
            except InvalidPage:
                return HttpResponse("")
            return render(request, request,
                                      'main/load_more_ajax.html',
                                      context)

        objects = paginator.page(1).object_list
        user_language = 'id'  # < this
        translation.activate(user_language)  # < this
        # < this
        request.session[translation.LANGUAGE_SESSION_KEY] = user_language
        context = dict(
            objects=objects,
            keyword=keyword,
            counting=counting,
            category=category,
            city=city,
            categories=Category.objects.all(),
            search_active=True,
        )
        context["cat"] = Category.objects.all()
        return render(request, 
            'main/search.html',
            context)


class TourDetail(View):
    template_name = 'main/experience_detail.html'

    def split_date(self, stringDate):
        try:
            stringDate = stringDate.split("-")
            stringDate = dt(int(stringDate[2]), int(
                stringDate[1]), int(stringDate[0]))
        except Exception:
            stringDate = False
        return stringDate

    def get_format_rupiah(self, price):
        return format_decimal(price, locale='ID')

    def get_price(self, obj, slug, date, guest):
        other_guest = 0
        for i in obj.booking_set.filter(~Q(status = 3), dates=date):
            other_guest += i.guest

        sharing_verify = True if (other_guest +
                                  guest) <= obj.tour.maxgroup else False

        if not sharing_verify:
           return False

        price_per = obj.price * guest
        percentage = 10000
        pricetotal = price_per + percentage
        price = obj.price

        dictdata = dict(
            price = self.get_format_rupiah(price),
            pricetotal = self.get_format_rupiah(pricetotal),
            percentage = self.get_format_rupiah(percentage),
            price_per = self.get_format_rupiah(price_per),
            other_guest = other_guest,
            sharing_verify = sharing_verify,
            obj = obj,
            guest = guest,
            date = date
        )
        return dictdata

    def get(self, request, *args, **kwargs):
        slug = kwargs['slug']
        user_language = 'id' 
        translation.activate(user_language) 
        request.session[translation.LANGUAGE_SESSION_KEY] = user_language

        data = request.GET
        if data.get('getprice'):
            template = "main/sharing_ajax.html"
            if not data.get("date"):
                return HttpResponse("nodate")
            date = self.split_date(data.get("date"))
            guest = int(data.get('guest', 1))
            tour = TourPackage.objects.get(slug=str(slug))
            filters = {
                'tour__slug': str(slug),
                'blocked': False,
                'start_date__lte': date,
                'end_date__gte':date
                
            }

            obj = Availability.objects.select_related("tour").prefetch_related("booking_set").only(
                "tour__maxgroup",
                "price",
                "start_time",
                "end_time",
                "price").filter(
                **filters).latest('created')


            dictdata = self.get_price(obj, slug, date, guest)
            if not dictdata:
               template = "main/not_available.html"
               return render(request, template, {})
            return render(request, template, dictdata)

        else:
            getobjects = TourPackage.objects.select_related(
              "city", "category", "secondary_category").prefetch_related("availability_set", "booking_set").get(slug=slug)

            context = dict(
                object=getobjects,
            )
            return render(request, self.template_name, context)


class BookingView(View):
    template_name = 'main/booking.html'

    def split_date(self, stringDate):
        try:
            stringDate = stringDate.split("-")
            stringDate = dt(int(stringDate[2]), int(
                stringDate[1]), int(stringDate[0]))
        except Exception as err:
            stringDate = dt.date.today()
        return stringDate

    def whatsupLink(self, describe):
        describe = quote(describe)
        return "https://api.whatsapp.com/send?phone=6285840025166&text=%s" % describe

    def get(self, request, *args, **kwargs):
        context = dict(
            prov=kwargs['prov'],
            TS=Booking.TS,
            BK=Booking.BK,
            city=kwargs['city'],
            dest=kwargs['dest']
        )
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        data = request.POST
        name = data.get("name")
        contact = data.get("phone")
        dates = self.split_date(data.get("dates"))
        guest = data.get("qtyInput", 1)
        slug = kwargs['slug']
        tour = TourPackage.objects.get(slug=slug)
        email = data.get("email")
        key = key_generator(limit=6)
        obj = Availability.objects.filter(
            tour=tour, blocked=False, start_date__lte=dates, end_date__gte=dates).latest('created')
        
        if not obj:
            return HttpResponse("dateblocked")

        
        exists, created = Booking.objects.get_or_create(
            tour=tour, available=obj, name=name, contact=contact, dates=dates, email=email, guest=guest)

        if created:
            exists.order_id = key
            exists.price = exists.get_price_raw
            exists.save()
            data = {
                'external_id': 'lokalx-11',  # % exists.order_id,
                'payer_email': email,
                'description': '%s untuk %s orang, tanggal %s' % (tour.title, exists.guest, data.get("dates")),
                'amount': '%s' % exists.get_price_raw,
                'should_send_email': 'true',
            }
            session = requests.Session()
            session.trust_env = False
            response = session.post(
                'https://api.xendit.co/v2/invoices',
                data=data,
                auth=(
                    'xnd_production_XL7JLXKq81uCg8SPcIuzw2rmyptP1MO8w3QfDimbP0WE8QUMI6W47mceqbNfFGo',
                    ''),
            )
            response = response.json()
            exists.payment_id = response['id']
            exists.save()
            url = response["invoice_url"]
            return HttpResponse(url)

        return HttpResponse(exists.get_link())


class ArticleView(View):
    template_name = 'main/blog.html'

    def get(self, request, *args, **kwargs):
        user_language = 'id'  # < this
        translation.activate(user_language)  # < this
        # < this
        request.session[translation.LANGUAGE_SESSION_KEY] = user_language

        objects = Article.objects.all()
        LIMIT = 5
        latestblog = objects.order_by("created")[:(LIMIT + 1)]
        if len(latestblog) > LIMIT:
            latestblog = objects.only(
                "created", "title", "photo").latest("created")

        object_list = objects.order_by('-id')
        items_per_page = 10
        data = request.GET

        page = data.get('page', '1')
        if not page.isdigit():
            page = 1
        page = int(page)

        paginator = Paginator(object_list, items_per_page)
        try:
            page = paginator.page(page)
        except EmptyPage:
            pass

        context = dict(
            page=page,
            paginator=paginator,
            lastest=latestblog,
            category=CategoryArticle.objects.only("name", "slug").all(),
        )
        return render(request, 
            self.template_name,
            context)


class Invoice(View):
    template_name = 'main/booking.html'

    def get(self, request, *args, **kwargs):
        booking = Booking.objects.get_object_or_404(
            order_id=request.GET.get('id'))

        context = {
            "booking_url": "https://lokalx.id/booking?id=%s" %
            booking.order_id,
            "booking_id": booking.order_id,
            "exp_title": booking.tour.title,
            "guest_name": booking.name,
            "number_of_guest": "%s orang" %
            booking.guest,
            "booking_date": booking.dates.strftime("%d %B %Y"),
            "url_contact_pramu": "https://wa.me/082338632990",
            "exp_url": "https://lokalx.id/experience/%s" %
            booking.tour.slug,
        }

        return render(request, 
            self.template_name,
            context)


class SuccessPayment(View):
    template_name = 'main/invoice.html'

    def post(self, request, *args, **kwargs):
        try:
            data = simplejson.loads(request.body)
            booking = Booking.objects.get(
                payment_id=data["id"], order_id=data['external_id'])
            booking.status = "2"
            booking.payment_method = data['payment_method']
            booking.paid_amount = data['paid_amount']
            booking.adjusted_received_amount = data['adjusted_received_amount']
            booking.paid_date = dateutil.parser.parse(data['created'])
            booking.json_objects = request.body
            booking.save()
            var_data = {
                "v:booking_url": "https://lokalx.id/booking?id=%s" %
                booking.order_id,
                "v:booking_id": booking.order_id,
                "v:exp_title": booking.tour.title,
                "v:guest_name": booking.name,
                "v:number_of_guest": "%s orang" %
                booking.guest,
                "v:booking_date": booking.dates.strftime("%d %B %Y"),
                "v:url_contact_pramu": "https://wa.me/082338632990",
                "v:exp_url": "https://lokalx.id/experience/%s" %
                booking.tour.slug,
                "o:tag": [
                    "Booking",
                    "Order",
                    "Pemesanan"]}
            origin_data = {
                "from": "Cinta - Lokalx.id <cinta@lokalx.id>",
                "to": booking.email,
                "subject": "Transaksi Pemesanan Pengalaman Berhasil: Kode Booking #%s" %
                booking.order_id,
                "template": "bookingtemplatev1"}

            origin_data.update(var_data)

            request = requests.post(
                "https://api.mailgun.net/v3/mail.lokalx.id/messages",
                auth=(
                    "api",
                    "ec5887cdca35ec1c706bf25b5215181d-f8b3d330-95b75c29"),
                data=origin_data)
        except Exception:
            return HttpResponse("no")
        return HttpResponse("yes")


class Cancellation(View):
    template_name = 'main/cancellation.html'

    def get(self, request, *args, **kwargs):
        user_language = 'id'  # < this
        translation.activate(user_language)  # < this
        # < this
        request.session[translation.LANGUAGE_SESSION_KEY] = user_language
        context = dict(
            object=[],
        )
        return render(request, 
            self.template_name,
            context)


class JoinLokalHost(View):
    template_name = 'main/joinlokalhost.html'

    def get(self, request, *args, **kwargs):
        user_language = 'id'  # < this
        translation.activate(user_language)  # < this
        # < this
        request.session[translation.LANGUAGE_SESSION_KEY] = user_language
        faq_pramuwisata = FaqPramuwisata.objects.all()
        context = dict(
            faq_pramuwisata=faq_pramuwisata,
            object_list=[],
        )
        return render(request, 
            self.template_name,
            context)


class BlogCategory(View):
    template_name = 'main/blog.html'

    def get(self, request, *args, **kwargs):
        user_language = 'id'  # < this
        translation.activate(user_language)  # < this
        # < this
        request.session[translation.LANGUAGE_SESSION_KEY] = user_language
        objects = Article.objects.all()
        LIMIT = 5
        latestblog = objects.order_by("created")[:(LIMIT + 1)]
        if len(latestblog) > LIMIT:
            latestblog = objects.only(
                "created", "title", "photo").latest("created")

        object_list = objects.filter(
            category__slug=kwargs['slug']).order_by('-id')
        items_per_page = 10
        data = request.GET

        page = data.get('page', '1')
        if not page.isdigit():
            page = 1
        page = int(page)

        paginator = Paginator(object_list, items_per_page)
        try:
            page = paginator.page(page)
        except EmptyPage:
            pass

        context = dict(
            page=page,
            paginator=paginator,
            lastest=latestblog,
            pagecategory=True,
            keyword=kwargs['slug'],
            category=CategoryArticle.objects.only("name", "slug").all(),
        )
        return render(request, 
            self.template_name,
            context)


class Policy(View):
    template_name = 'main/policy.html'

    def get(self, request, *args, **kwargs):
        user_language = 'id'  # < this
        translation.activate(user_language)  # < this
        # < this
        request.session[translation.LANGUAGE_SESSION_KEY] = user_language
        context = dict(
            object_list=[],
        )
        return render(request, 
            self.template_name,
            context)


class Faqs(View):
    template_name = 'main/faq.html'

    def get(self, request, *args, **kwargs):
        user_language = 'id'  # < this
        translation.activate(user_language)  # < this
        # < this
        request.session[translation.LANGUAGE_SESSION_KEY] = user_language

        faq_wisatawan = FaqWisatawan.objects.all()
        faq_pramuwisata = FaqPramuwisata.objects.all()
        context = dict(
            faq_wisatawan=faq_wisatawan,
            faq_pramuwisata=faq_pramuwisata,
            object_list=[]
        )
        return render(request, 
            self.template_name,
            context)


class AboutView(View):
    template_name = 'main/about.html'

    def get(self, request, *args, **kwargs):
        user_language = 'id'  # < this
        translation.activate(user_language)  # < this
        # < this
        request.session[translation.LANGUAGE_SESSION_KEY] = user_language
        return render(request, 
            self.template_name)


class ContactView(View):
    template_name = 'main/contact.html'

    def get(self, request, *args, **kwargs):
        user_language = 'id'  # < this
        translation.activate(user_language)  # < this
        # < this
        request.session[translation.LANGUAGE_SESSION_KEY] = user_language
        return render(request, 
            self.template_name)


class ArticleDetail(View):
    template_name = 'main/blog_detail.html'

    def get(self, request, *args, **kwargs):
        user_language = 'id'  # < this
        translation.activate(user_language)  # < this
        # < this
        request.session[translation.LANGUAGE_SESSION_KEY] = user_language
        context = dict(
            object=Article.objects.get(slug=kwargs['slug']),
        )
        return render(request, 
            self.template_name,
            context)
