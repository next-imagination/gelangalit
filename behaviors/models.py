from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import pre_save
from django.http.response import Http404, HttpResponse

def human_format(num):
    num = float('{:.3g}'.format(num))
    magnitude = 0
    while abs(num) >= 10000:
        magnitude += 1
        num /= 1000.0
    return '{}{}'.format('{:f}'.format(num).rstrip('0').rstrip('.'), ['', 'K', 'M', 'B', 'T'][magnitude])

class ObjectManager(models.Manager):
    def get_or_none(self, **kwargs):
        try:
            return self.get(**kwargs)
        except self.model.DoesNotExist:
            return None     

    def get_object_or_404(self, **kwargs):
        try:
            return self.get(**kwargs)
        except self.model.DoesNotExist:
            raise Http404('No %s matches the given query.' % self.model._meta.object_name)

    def get_list_or_404(self, **kwargs):
        obj_list = list(self.filter(**kwargs))
        if not obj_list:
           raise Http404('No %s matches the given query.' % self.model._meta.object_name)
        return obj_list

    
class Timestampable(models.Model): 
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    class Meta: 
        abstract = True
 
class Authorable(models.Model):
    author = models.ForeignKey(User)

    class Meta:
        abstract = True

class Nameable(models.Model):
    name = models.CharField(max_length=200, unique=True)
    class Meta:
        abstract = True

class Permalinkable(models.Model):
    slug = models.SlugField(blank=True, max_length=500)

    class Meta:
        abstract = True

    def get_url_kwargs(self, **kwargs):
        kwargs.update(getattr(self, 'url_kwargs', {}))
        return kwargs

    @models.permalink
    def get_absolute_url(self):
        url_kwargs = self.get_url_kwargs(slug=self.slug)        
        return (self.url_name, (), url_kwargs)

    #def pre_save(self, instance, add):
    #    from django.utils.text import slugify
    #    if not instance.slug:
    #        instance.slug = slugify(self.slug_source)

class Publishable(models.Model):
    publish_date = models.DateTimeField(null=True)

    class Meta:
        abstract = True

    def publish_on(self, date=None):
        from django.utils import timezone
        if not date:
            date = timezone.now()
        self.publish_date = date
        self.save()

    @property
    def is_published(self):
        from django.utils import timezone
        return self.publish_date < timezone.now()
