from django.contrib import admin
from django.forms import ModelForm
from blog.models import Article
from django.template.loader import render_to_string, get_template
from django.utils.safestring import mark_safe


class ArticleAdmin(admin.ModelAdmin):
    fieldExclude = ["id", "content", "slug", "photo"]
    list_display = [
        field.name for field in Article._meta.fields if field.name not in fieldExclude]
    #list_display += ["bphoto"]
    search_fields = ('title',)
    ordering = ('-id',)
    list_filter = ("category", "is_publish", "pin")
    #readonly_fields = ["photo_image", "slug"]

    def changelist_view(self, request, extra_context=None):
        extra_context = {
            'title': 'List Data Artikel. Cari, Ubah dan Delete.'}
        return super(ArticleAdmin, self).changelist_view(request, extra_context=extra_context)


    #def bphoto(self, obj):
    #    return render_to_string('thumb.html', {
    #        'image': obj.photo
    #    })

    #bphoto.allow_tags = True

    #def photo_image(self, obj):
    #    return mark_safe('<img src="{url}" width="800"/>'.format(
    #         url=obj.photo.url,
    #     )
    #    )

admin.site.register(Article, ArticleAdmin)
