# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.contrib import admin
from django.template import defaultfilters
from tinymce import HTMLField
import os
from datetime import datetime as dt
from behaviors.models import (
    human_format,
    ObjectManager,
    Timestampable,
    Authorable,
    Permalinkable,
    Nameable
)


class CategoryArticle(Nameable, Timestampable, Permalinkable, models.Model):
    url_name = "category"

    @property
    def slug_source(self):
        return self.name

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = defaultfilters.slugify(self.name)
        super(CategoryArticle, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Kategori'

admin.site.register(CategoryArticle)


def get_article_upload(instance, filename):
    return os.path.join('blog/', dt.now().date().strftime("%Y/%m/%d"), filename)


class Article(Timestampable, Authorable, Permalinkable, models.Model):
    title = models.CharField(max_length=300)
    content = HTMLField('Content')
    category = models.ForeignKey(CategoryArticle, blank=True, null=True)
    pin = models.BooleanField(default=False)
    is_publish = models.BooleanField(default=True)
    photo = models.ImageField(
        upload_to=get_article_upload, blank=True, null=True)

    # def __str__(self):
    # return "%s: %s" % (self.title.encode('utf-8').decode('utf-8'),
    # self.author)

    objects = ObjectManager()
    url_name = "experience"

    @property
    def slug_source(self):
        return self.title

    def get_category(self):
        return self.category.name

    def get_photo(self):
        return "/media/%s" % self.photo.url

    def get_cover(self):
        return "/media/%s" % self.cover.url

    def get_price(self):
        return human_format(self.price)

    def __str__(self):
        if self.is_publish:
            return "[ACTIVE] %s" % (self.title)
        return "[DISABLE] %s" % (self.title)

    def save(self, *args, **kwargs):
        if self.title:
            slugs = defaultfilters.slugify(self.title[:100])
            try:
                Article.objects.get(slug=self.slug)
            except:
                self.slug = slugs
            super(Article, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Posting Artikel'
