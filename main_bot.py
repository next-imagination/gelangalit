from __future__ import absolute_import
from config import base
import os, re
os.environ['DJANGO_SETTINGS_MODULE'] = 'config.base'
from telegram.ext import Updater, CommandHandler
from tour.models import Booking

def hello(bot, update):
    update.message.reply_text(
        'Hello {}'.format(update.message.from_user.first_name))
    
def unpaid_booking(bot, update):
    get_booking = Booking.objects.filter(status=False)
    count = get_booking.count()
    update.message.reply_text('Total %s' % count)
    if count:
       text = ""     
       update.message.reply_text('Ini adalah daftar booking yang belum dibayar')
       for i in get_booking:
           text += """
Booking #00%s
Experience: %s 
Tanggal Booking: %s
Tanggal Booking di buat: %s
Link: %s 
           """ % (i.id, i.tour.title, i.dates.strftime("%d/%m/%Y"), i.created.strftime("%d/%m/%Y"), i.get_link())
       update.message.reply_text(text)


def paid_booking(bot, update):
    get_booking = Booking.objects.filter(status=True)
    count = get_booking.count()
    update.message.reply_text('Total %s' % count)
    if count:
       text = ""     
       update.message.reply_text('Ini adalah daftar booking yang telah dibayar')
       for i in get_booking:
           text += """
Booking #00%s
Experience: %s 
Tanggal Booking: %s
Tanggal Booking di buat: %s
Link: %s 
           """ % (i.id, i.tour.title, i.dates.strftime("%d/%m/%Y"), i.created.strftime("%d/%m/%Y"), i.get_link())
       update.message.reply_text(text)


   
updater = Updater('743973008:AAEYaIq59sRLQIZ6Z1OaFYqCkr5c6iYZgaY')
updater.dispatcher.add_handler(CommandHandler('hello', hello))
updater.dispatcher.add_handler(CommandHandler('unpaid_booking', unpaid_booking))
updater.dispatcher.add_handler(CommandHandler('paid_booking', paid_booking))

updater.start_polling()
updater.idle()
